require 'faraday'
require 'faraday_middleware'
require 'active_support/all'
require 'toolbox/version'
require "nokogiri"

Dir[File.expand_path('../../faraday/*.rb', __FILE__)].each{|f| require f}
require File.expand_path('../toolbox/configuration', __FILE__)
require File.expand_path('../toolbox/api', __FILE__)
require File.expand_path('../toolbox/client', __FILE__)
require File.expand_path('../toolbox/xml_reader', __FILE__)

module Toolbox

  extend Configuration
  # Alias for Toolbox::Client.new
  # @return [Toolbox::Client]
  def self.client(options = {})
    Toolbox::Client.new(options)
  end

  # Delegate to Toolbox::Client
  def self.method_missing(method, *args, &block)
    return super unless client.respond_to?(method)
    client.send(method, *args, &block)
  end
end
