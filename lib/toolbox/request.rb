module Toolbox
  # Defines HTTP request methods
  module Request
    DEFAULT_STARTDATE = "1/1/2010".freeze

    # Perform an HTTP GET request
    def get(path, options = {})
      request(:get, path, options)
    end

    # Perform an HTTP POST request
    def post(path, options = {})
      request(:post, path, options)
    end

    private

    # Perform an HTTP request
    def request(method, path, options)
      response = connection.send(method) do |request|
        case method
        when :get
          formatted_options = format_options(options)
          request.url(path,formatted_options)
        when :post, :put
          request.headers['Content-Type'] = 'application/json'
          request.body = options.to_json unless options.empty?
          request.url(path)
        end
      end

      method = caller[1][/`.*'/][1..-2]
      Response.create(response.body, method)
    end

    # TODO: Replace these options with whatever options you need to pass for the target API.
    # Format the Options before you send them off to Toolbox
    def format_options(options)
      options[:startDate]   = options[:startDate] ? format_fields(options[:startDate]) : DEFAULT_STARTDATE
      options[:endDate]     = options[:endDate] ? format_fields(options[:endDate]) : Time.now.strftime("%d-%m-%Y")
      return options
    end

    # TODO: Replace or remove this method if necessary.
    # Format the fields to a format that the Toolbox likes
    # @param [Array or String] fields can be specified as an Array or String
    # @return String
    def format_fields(fields)
      return CGI.escape(fields)
    end
  end
end
