module Toolbox
  class XmlReader
    module Transactions

      # @return [Array] of hashes representing transactions
      # @param xml_doc [NodeSet] Nokogiri XML nodeset
      # Parses the transaction information from the NodeSet and organizes it into an array of hashes
      def parse_transactions(xml_doc)
        selected_nodes = xml_doc.css('string Franchise Customer').select{|n| n.css('Project').count>0}

        @transactions = []

        selected_nodes.map do |node|
          project_nodes = node.css('Project')
          project_nodes.each do |project_node|

            project_id = project_node.attributes['Project_ID'].value

            jobs = project_node.css('Job[Status=Active]')

            completed_count = get_completed_count(jobs)
            total_count = project_node.css('Job[Status=Active]').size
            is_paid = completed_count == total_count ? '1' : '0'

            max_date = get_max_date(jobs)

            amount = get_amount_sum(jobs)

            @transactions << {
              external_id:         project_id,
              external_contact_id: node['ID'],
              receipt_number:      project_id,
              due_date:            max_date,
              transacted_at:       max_date,
              amount:              amount,
              is_paid:             is_paid,
            }
          end
        end

      end

      private

      # @return [BigDecimal] a count of the # of completed jobs
      # @param jobs [NodeSet] of the jobs for this particular customer
      # Get the count of jobs that have a JobTotal value, indicating that it's completed.
      def get_completed_count(jobs)
        jobs.select{|j| !j['JobTotal'].empty? and BigDecimal.new(j['JobTotal'])}.count
      end

      # @return [DateTime] The latest date for TransmitDateTimeHO
      # @param jobs [NodeSet] of the jobs for this particular customer
      def get_max_date(jobs)

        dates = jobs.map {|j| j['ActualStartTime'] }
        dates = dates.select{|j| !j.nil? }
        return nil if dates.empty?
        max_date = dates.map{|j| DateTime.parse(j)}.sort.last

        return max_date
      end

      # @return [BigDecimal] the sum of the JobTotal values that's arent' empty
      # @param jobs [NodeSet] of the jobs for this particular customer
      def get_amount_sum(jobs)
        jobs.select{|j| !j['JobTotal'].empty?}.map{|j| BigDecimal.new(j['JobTotal'])}.inject(0){|sum, n| sum+n}
      end
    end
  end
end
