module Toolbox
  class XmlReader
    module Customers

      # @params xml_doc [Nokogiri::XML] Nokogiri XML nodeset
      # parses the needed areas of the XML nodeset and transforms that information into an array of hashes
      # each representing a customer.
      def parse_customers(xml_doc)
        @customers = xml_doc.css('string Franchise Customer').map do |node|
          first_name, last_name, company_name = handle_name(node.css('SortName').text, node.at_css('Company').text)
          wants_communication = get_value(node, 'DoNotContact') == "0" ? "1" : "0"

          {
            external_id:          node['ID'],
            first_name:           first_name,
            last_name:            last_name,
            email:                get_value(node, 'EmailAddress'),
            phone_daytime:        get_value(node, 'Phone1'),
            phone_daytime_ext:    nil,
            phone_mobile:         get_value(node, 'Phone2'),
            address1:             get_value(node, 'Address1'),
            address2:             get_value(node, 'Address2'),
            city:                 get_value(node, 'City'),
            state:                get_value(node, 'State'),
            zip:                  get_value(node, 'Zip'),
            country:              nil,
            wants_communication:  wants_communication,
            commercial:           company_name.nil? ? '0' : '1',
            company_name:         company_name,
          }
        end
      end

      private

      # @return [String]
      # @param node [Nokogiri::Node] Node to check the value of
      # @param field_name [String] field in the node we're interested in
      # Helper function taht takes a node and field name, returning the value of the field or nil
      def get_value(node, field_name)
        value = node.at_css(field_name)

        if value.nil? or value.text.empty?
          return nil
        else
          return value.text
        end
      end

      # @return [Array] of length 3, all values either nil or a strings
      # @param string_name [String] name of the person
      # @param company_name [String] name of the company
      # Determines whether the customer is a company or a person and then returns the name of that
      # entity.  If it's a customer, the format is "lastName, firstName" so we handle that via splitting.
      def handle_name(string_name, company_name)
        if company_name.nil? || company_name == ""
          split = string_name.split(',')
          return [split[1].strip, split[0].strip, nil] if split.length >= 2
          return [string_name, nil, nil]
        else
          return [company_name, nil, company_name]
        end
      end
    end
  end
end
