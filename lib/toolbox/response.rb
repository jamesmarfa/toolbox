module Toolbox
  module Response
    def self.create(response_hash, method)
      return Toolbox::XmlReader.new(response_hash, method)
    end

    attr_reader :pagination
    attr_reader :meta
  end
end
