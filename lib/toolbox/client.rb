module Toolbox
  # Wrapper for the Toolbox REST API.
  class Client < API
    Dir[File.expand_path('../client/*.rb', __FILE__)].each{|f| require f}

    # TODO: Be sure to update these includes to the actual resources available in the client directory.
    include Toolbox::Client::Customers
    include Toolbox::Client::Transactions
  end
end
