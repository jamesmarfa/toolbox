module Toolbox
  class Client
    module Transactions

      def transactions(params = {})
        response = get("GetCustomerListByFranNum", params)
      end

      def transaction(id, params = {})
        response = get("transactions/#{id}", params)
      end

    end
  end
end
