module Toolbox
  class Client
    module Customers

      def customers(params = {})
        response = get("GetCustomerListByFranNum", params)
      end

      def customer(id, params = {})
        response = get("customers/#{id}", params)
      end

    end
  end
end
