require 'bigdecimal'

module Toolbox

  # XmlReader is used to encapsulte as much as possible of the resulting XML parsing
  # It utilitizes a Nokogiri Nodeset to read the XML
  # and then converts it to 2 arrays of hashes (customers, transactions)
  class XmlReader
    Dir[File.expand_path('../xml_reader/*.rb', __FILE__)].each{|f| require f}


    attr_accessor :customers, :transactions

    # @return [XmlReader] with the xml_string parsed into instance variables
    # @param xml_string [String] Xml String with the structure provided by the Toolbox API
    # @param parse_type [String] String to identify parse
    def initialize(xml_string, parse_type)
      @parse_type = parse_type
      parse_xml(xml_string)
    end

    private

    # @return nil
    # @param xml_string [String] Xml string from Toolbox API response.body
    def parse_xml(xml_string)
      xml_doc = Nokogiri::XML(xml_string)
      send("parse_#{@parse_type}", xml_doc)
    end

    include Toolbox::XmlReader::Customers
    include Toolbox::XmlReader::Transactions
  end
end
