require 'spec_helper'

RSpec.describe Toolbox do
  after do
    Toolbox.reset
  end

  describe ".client" do
    it "should be a Toolbox::Client" do
      expect(Toolbox.client).to be_a(Toolbox::Client)
    end
  end

  describe '#api_key' do
    it 'should return the default api key' do
      expect(Toolbox.api_key).to eq(Toolbox::Configuration::DEFAULT_API_KEY)
    end
  end

  describe '#api_key=' do
    it 'should set the api key' do
      Toolbox.api_key = 'test'
      expect(Toolbox.api_key).to eq('test')
    end
  end

  describe '#api_version' do
    it 'should return the default api version' do
      expect(Toolbox.api_version).to eq(Toolbox::Configuration::DEFAULT_API_VERSION)
    end
  end

  describe '#api_version=' do
    it 'should set the api_version' do
      Toolbox.api_version = '/test'
      expect(Toolbox.api_version).to eq('/test')
    end
  end

  describe '#adapter' do
    it 'should return the default adapter' do
      expect(Toolbox.adapter).to eq(Toolbox::Configuration::DEFAULT_ADAPTER)
    end
  end

  describe '#adapter=' do
    it 'should set the adapter' do
      Toolbox.adapter = :typhoeus
      expect(Toolbox.adapter).to eq(:typhoeus)
    end
  end

  describe '#endpoint' do
    it 'should return the default endpoint' do
      expect(Toolbox.endpoint).to eq(Toolbox::Configuration::DEFAULT_ENDPOINT)
    end
  end

  describe '#endpoint=' do
    it 'should set the endpoint' do
      Toolbox.endpoint = 'http://www.google.com'
      expect(Toolbox.endpoint).to eq('http://www.google.com')
    end
  end

  describe '#configure' do
    Toolbox::Configuration::VALID_OPTIONS_KEYS.each do |key|

      it "should set the #{key}" do
        Toolbox.configure do |config|
          config.send("#{key}=", key)
          expect(Toolbox.send(key)).to eq(key)
        end
      end
    end
  end
end
