require 'spec_helper'
RSpec.describe Toolbox::XmlReader do

  context "when parse by customers" do
    it "should set customers data" do
      @customer = Toolbox::XmlReader.new(fixture('xml_test.xml'), 'customers')
      expect(@customer.transactions).to_not eq []
    end
  end

  context "when parse by transactions" do
    it "should set transactions data" do
      @transactions = Toolbox::XmlReader.new(fixture('xml_test.xml'), 'transactions')
      expect(@transactions.transactions).to_not eq []
    end
  end
end
