require 'spec_helper'

RSpec.describe Toolbox::Client::Transactions do
  before do
    @client = Toolbox::Client.new
  end

  describe '#transactions' do

    before do
      stub_get("GetCustomerListByFranNum?accessKey=&endDate=02%252F07%252F2015&franName=2889&startDate=1%252F1%252F2010&zorNam=1").to_return(body: fixture('transactions_list.json'), :headers => {:content_type => "application/json; charset=utf-8", authorization: 'Basic blah'})
    end

    it 'should return a list of orders' do
      @client.transactions(startDate: "1/1/2010", endDate:  "02/07/2015", zorNam: "1", franName: "2889")
      expect(a_get("GetCustomerListByFranNum?accessKey=&endDate=02%252F07%252F2015&franName=2889&startDate=1%252F1%252F2010&zorNam=1")).to have_been_made
    end
  end

end
