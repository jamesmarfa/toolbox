require 'spec_helper'

RSpec.describe Toolbox::XmlReader::Customers do
  before do
    @reader = Toolbox::XmlReader.new('test', 'customers')
  end

  describe '#customers' do
    it 'should set customers list' do
      doc = Nokogiri::XML(fixture('xml_test.xml'))
      @reader.parse_customers(doc)
      expect(@reader.customers).to_not eq []
    end
  end

end
