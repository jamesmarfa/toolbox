require 'spec_helper'

RSpec.describe Toolbox::XmlReader::Transactions do
  before do
    @reader = Toolbox::XmlReader.new('test', 'transactions')
  end

  describe '#parse_transactions' do
    it 'should set customers list' do
      doc = Nokogiri::XML(fixture('xml_test.xml'))
      @reader.parse_transactions(doc)
      expect(@reader.transactions).to_not eq []
    end
  end

end
